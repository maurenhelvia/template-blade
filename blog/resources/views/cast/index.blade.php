@extends('adminlte.master')

@section('judul')
Data Pemain
@endsection

@section('judul-table')
Data Pemain
@endsection

@section('content-table')
<a href="/cast/create" class="btn btn-success">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('delete')
                                <a href="/cast/{{$value->id}}" class='btn btn-primary btn-sm my-2'>Detail<a>
                                    <a href="/cast/{{$value->id}}/edit" class='btn btn-success btn-sm my-2'>Edit<a>
                                <input type="submit" class="btn btn-danger btn-sm my-2" value="delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection