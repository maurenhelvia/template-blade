@extends('adminlte.master')

@section('judul')
Detail Pemain
@endsection

@section('judul-table')
Detail Pemain {{$cast->nama}}
@endsection

@section('content-table')
<h2>{{$cast->nama}}</h2>
<h4>{{$cast->umur}}</h4>
<p>{{$cast->bio}}</p>
@endsection